import { Component, OnInit } from '@angular/core';

import { Timeliner } from 'timeliner'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  ngOnInit() {
    const dimensions = {
      width: 1500,
      height: 300
    };
    // tooltip message config
    const tooltipMessageOfSum = "custom tooltip message";

    const startDate = new Date("august 31, 2019 03:24:00");
    const endDate = new Date("september 26, 2019 03:24:00");
    const dateSetDateRange = [startDate, endDate];

    const weightRange = [100, 100];
    const dataItem1 = Timeliner.generateDummyData(dateSetDateRange, weightRange, 100);

    const timelinerInstance = Timeliner.create("container", dimensions, tooltipMessageOfSum);
    timelinerInstance.createSet(dataItem1, "a", "magenta");

    timelinerInstance.bindEvents("set-checkbox-changed", e => console.log("setCheckboxChanged", e));
    timelinerInstance.bindEvents("color-changed", e => console.log("color changed", e));
    timelinerInstance.bindEvents("set-removed", e => console.log("setRemoved", e));
    timelinerInstance.bindEvents("tmp-range-select", e => console.log("tmpRangeSelect", e));
    timelinerInstance.bindEvents("tmp-range-unselect", e => console.log("tmpRangeUnselect", e));
    timelinerInstance.bindEvents("range-select-start", e => console.log("rangeSelectStart", e));
    timelinerInstance.bindEvents("range-select-end", e => console.log("rangeSelectEnd", e));
    timelinerInstance.bindEvents("range-selected", e => console.log("rangeSelected", e));
    timelinerInstance.bindEvents("range-unselected", e => console.log("rangeUnselected", e));
    timelinerInstance.bindEvents("selection-deleted", e => console.log("selectionDeleted", e));
    timelinerInstance.bindEvents("range-change-end", e => console.log("rangeChangeEnd", e));
    timelinerInstance.bindEvents("range-change-start", e => console.log("rangeChangeStart", e));
    timelinerInstance.bindEvents("range-drag-start", e => console.log("rangeDragStart", e));
    timelinerInstance.bindEvents("range-drag-end", e => console.log("rangeDragEnd", e));
    timelinerInstance.bindEvents("selection-drag", e => console.log("selectionDrag", e));
    timelinerInstance.bindEvents("zoom-start", e => console.log("zoomStart", e));
    timelinerInstance.bindEvents("zoom-end", e => console.log("zoomEnd", e));
    timelinerInstance.bindEvents("drag-start", e => console.log("dragStart", e));
    timelinerInstance.bindEvents("drag-end", e => console.log("dragEnd", e));
  }

  title = 'timeliner-angular';
}
